import os

import tango

os.environ["TANGO_HOST"] = "localhost:10000"
db = tango.Database()
device = tango.DbDevInfo()
device.name = "my/test/server"
device._class = "TestServer"
device.server = "TestServer/0"
db.add_device(device)
