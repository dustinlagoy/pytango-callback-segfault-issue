import time

import tango.server


class TestServer(tango.server.Device):
    @tango.server.attribute
    def good(self) -> float:
        return 1.0

    @tango.server.attribute
    def bad(self) -> float:
        time.sleep(3.5)
        raise RuntimeError("bad")


TestServer.run_server()
