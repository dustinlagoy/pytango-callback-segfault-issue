import time

import tango
import tango.utils

print(tango.utils.info())

tango.ApiUtil.instance().set_asynch_cb_sub_model(tango.cb_sub_model.PUSH_CALLBACK)


def callback(read_event):
    print("got response")
    for attribute in read_event.argout:
        print(attribute)


proxy = tango.DeviceProxy("my/test/server")


# this works as expected
print(proxy.good)

# this throws an error as expected
try:
    print(proxy.bad)
except tango.DevFailed:
    print("server.bad error")

# this throws an error as expected
id = proxy.read_attribute_asynch("bad")
while True:
    try:
        print(proxy.read_attribute_reply(id))
        break
    except tango.AsynReplyNotArrived:
        print("wait")
    except tango.DevFailed as error:
        print(error)
        break
    time.sleep(0.5)

# this segfaults
proxy.read_attribute_asynch("bad", callback)
while True:
    print("wait")
    time.sleep(1)
