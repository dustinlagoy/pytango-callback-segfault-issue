#include <iostream>
#include <chrono>
#include <thread>
#include <tango.h>


void read_synchronous(Tango::DeviceProxy* proxy, std::string name) {
  Tango::DeviceAttribute result;
  double value;
  try {
    result = proxy->read_attribute(name);
  } catch (Tango::CommunicationFailed) {
    std::cout << "Synchronous read \"" << name << "\" timed out" << std::endl;
    return;
  }
  result >> value;
  std::cout << "Synchronous read \"" << name << "\": " << value << std::endl;
}


void read_polling(Tango::DeviceProxy* proxy, std::string name) {
  Tango::DeviceAttribute* result;
  double value;
  long request_id = proxy->read_attribute_asynch(name);
  while (1 == 1) {
    try {
      result = proxy->read_attribute_reply(request_id);
      break;
    } catch (Tango::AsynReplyNotArrived) {}
    catch (Tango::CommunicationFailed) {
      std::cout << "Asynchronous polling read \"" << name << "\" timed out" << std::endl;
      return;
    }
  }
  *result >> value;
  std::cout << "Asynchronous polling read \"" << name << "\": " << value << std::endl;

}


class Callback : public Tango::CallBack {
 public:
  void attr_read(Tango::AttrReadEvent* event) {
    if (event->err) {
      std::cout << "Asynchronous callback read \"" << event->attr_names[0];
      std::cout << "\" error(s):";
      for (int i = 0; i < event->errors.length(); i++) {
        std::cout << " " << event->errors[i].reason;
      }
      std::cout << std::endl;
    } else {
      double value;
      event->argout[0][0] >> value;
      std::cout << "Asynchronous callback read \"" << event->attr_names[0];
      std::cout << "\": " << value << std::endl;
    }
  } 
};


void read_callback(Tango::DeviceProxy* proxy, std::string name) {
  Callback* callback = new Callback;
  proxy->read_attribute_asynch(name, *callback);
}


int main() {
  // Tango::ApiUtil::instance()->set_asynch_cb_sub_model(Tango::cb_sub_model::PULL_CALLBACK);
  Tango::ApiUtil::instance()->set_asynch_cb_sub_model(Tango::cb_sub_model::PUSH_CALLBACK);
  Tango::DeviceProxy *proxy = new Tango::DeviceProxy("my/test/server");

  read_synchronous(proxy, "good");
  read_synchronous(proxy, "bad");
  read_polling(proxy, "good");
  read_polling(proxy, "bad");
  read_callback(proxy, "good");
  read_callback(proxy, "bad");

  // wait for asynchronous replies
  int seconds_passed = 0;
  while ( seconds_passed < 5 ) {
    std::this_thread::sleep_for(1s);
    // if using PULL_CALLBACK
    // Tango::ApiUtil::instance()->get_asynch_replies();
    seconds_passed++;
  }
  
  return 0;
}
